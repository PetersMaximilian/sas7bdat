data mixedvalues_compressed_binary(compress=binary);
input integer float text $ time :TIME. date :yymmdd10.;
format time :TIME. date yymmdd10.;
datalines;
1 0.1 abc 00:00 1980-01-1
2 0.2 def 02:22 1990-12-31
3 0.3 GHI 23:59 2004-02-29
;

