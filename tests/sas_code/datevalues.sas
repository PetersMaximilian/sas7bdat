data datevalues;
  input datevalues :yymmdd10.;
format datevalues yymmdd10.;
datalines;
1900-01-01
1950-01-01
1960-01-01
1970-01-01
1980-01-01
1990-01-01
2000-01-01
2010-01-01
2020-01-01
2004-02-29
1999-12-31
;
